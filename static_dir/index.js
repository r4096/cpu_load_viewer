    function ready() {
    graf(0, 'container1', 'c1');
    graf(0, 'container2', 'c2');
}

function graf(a, b, canva) {

    var canvas = document.getElementById(canva);
    const c = canvas.getContext('2d');
    //Определяем необходимые данные
    proportions = {}
    var width = 1920 * 0.9;
    var height = 1080 * 0.5;
    var margin = 40;
    //Задаем ширину и высоту канвасу
    canvas.width = width;
    canvas.height = height;

    var data = [];
    var maxx = 0;
    for (var i = 0; i < a.length; i++) {
        var col = '#3b3c36';
        data.push(
            {
                x: maxx,
                y: a[i],
                color: col
            }
        )
        maxx += 5

    }


    var xMinValue = getMin(data, function (d) {
        return d.x;
    });
    var xMaxValue = getMax(data, function (d) {
        return d.x;
    });
    var yMinValue = getMin(data, function (d) {
        return d.y
    });
    var yMaxValue = getMax(data, function (d) {
        return d.y
    });

    var xAxisLength = width - 2 * margin;
    var yAxisLength = height - 2 * margin;

    var xAxisStart = 0;
    var xAxisEnd = maxx;
    var yAxisStart = 0;
    var yAxisEnd = 100;

    //Определяем разницу значений величин по осям
    var rangeValuesX = xAxisEnd - xAxisStart;
    var rangeValuesY = yAxisEnd - yAxisStart;
    //Определяем коэффициенты для нахождения координат значений по осям
    var factorPositionX = xAxisLength / rangeValuesX;
    var factorPositionY = yAxisLength / rangeValuesY;

    createAxisLine(width, height, margin);
    outputValuesAxis();
    createLineGraph();


    function createAxisLine(width, height, margin) {
        var indentAxis = 10;

        var xAxisX_1 = margin - indentAxis;
        var xAxisY_1 = margin;
        var xAxisX_2 = xAxisX_1;
        var xAxisY_2 = height - margin;

        var yAxisX_1 = margin;
        var yAxisY_1 = (height - margin) + indentAxis;
        var yAxisX_2 = width - margin;
        var yAxisY_2 = yAxisY_1;

        c.beginPath();
        c.moveTo(xAxisX_1, xAxisY_1);
        c.lineTo(xAxisX_2, xAxisY_2);
        c.stroke();

        c.beginPath();
        c.moveTo(yAxisX_1, yAxisY_1);
        c.lineTo(yAxisX_2, yAxisY_2);
        c.stroke();
    }

    function outputValuesAxis() {
        //Дефолтные величины
        var stepWidth = 50;
        var indent = 20;

        //Определяем количество шагов на каждой из оси
        var amountStepsX = Math.round(xAxisLength / stepWidth);
        var amountStepsY = Math.round(yAxisLength / stepWidth);
        //Определяем коэффициенты для нахождения значений по осям
        var factorValueX = rangeValuesX / amountStepsX;
        var factorValueY = rangeValuesY / amountStepsY;


        c.beginPath();
        c.font = "10px Arial";
        c.textAlign = "center";
        c.textBaseline = "top";

        for (var i = 0; i < amountStepsX; i++) {
            var valueAxisX = Math.round(xAxisStart + i * factorValueX);
            var positionX = scaleX(valueAxisX);
            var positionY = (height - margin + indent);
            c.fillText('', positionX, positionY);
        }


        c.beginPath();
        c.font = "10px Arial";
        c.textAlign = "end";
        c.textBaseline = "middle";

        for (var i = 0; i < amountStepsY; i++) {
            var valueAxisY = Math.round(yAxisStart + i * factorValueY);
            var positionX = margin - indent;
            var positionY = scaleY(valueAxisY);
            c.fillText(valueAxisY + '%', positionX, positionY);
        }


    }

    function createLineGraph() {
        for (var i = 0; i < data.length; i++) {
            if (i != data.length - 1) { // Если элемент не последний
                var currentX = data[i].x;
                var currentY = data[i].y;
                var nextX = data[i + 1].x;
                var nextY = data[i + 1].y;

                c.beginPath();
                c.strokeStyle = data[i].color;
                c.lineWidth = 2
                c.moveTo(scaleX(currentX), scaleY(currentY));
                c.lineTo(scaleX(nextX), scaleY(nextY));

                c.stroke();
            }
        }
    }

    function scaleX(value) {
        return ((factorPositionX * value) + margin) - (xAxisStart * factorPositionX);
    }

    function scaleY(value) {
        return (height - (factorPositionY * value) - margin) + (yAxisStart * factorPositionY);
    }

    function getMin(data, callback) {
        var arr = [];
        for (var i in data) {
            arr.push(callback(data[i]));
        }
        return Math.min.apply(null, arr);
    }

    function getMax(data, callback) {
        var arr = [];
        for (var i in data) {
            arr.push(callback(data[i]));
        }
        return Math.max.apply(null, arr);
    }

}

document.addEventListener("DOMContentLoaded", ready);

setInterval(() => {
    fetch('/get_data').then((response) => {
        return response.json();
    }).then((data) => {
        var data_1 = data

        var a = Object.values(data_1)


        if (a.length / 12 >= 1) {


            var b = []
            let int_a = Math.floor(a.length / 12)
            for (let t = 0; t < int_a; t++) {

                let cont = 0
                for (let i = 0; i < 12; i++) {
                    cont += parseInt(a[i + 12 * t])
                }
                cont = Math.floor(cont / 12)
                b.push(cont)
            }

            let b_span = document.getElementById('b_span')
            b_span.innerHTML = ' — ' + b[b.length - 1] + ' %'
            graf(b, "container2", 'c2');
        }
        let a_span = document.getElementById('a_span')

        a_span.innerHTML = ' — ' + a[a.length - 1] + ' %'

        graf(a, "container1", 'c1');

    });
}, 5050)