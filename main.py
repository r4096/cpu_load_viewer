import psutil as psutil
from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc
import datetime

app = Flask(__name__, static_folder="static_dir")
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///cpu_load.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


# создаем объект базы данных
class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cpuload = db.Column(db.String)
    time = db.Column(db.DateTime, default=datetime.datetime.now())

    def __repr__(self):
        return self.id


# формируем корневую страницу
@app.route('/')
def index():
    return render_template('index.html')


# формируем отдельный канал для прослушивания
@app.route('/get_data', methods=['POST', 'GET'])
def post_data():
    # GET request
    if request.method == 'GET':
        past_hour = datetime.datetime.today() - datetime.timedelta(hours=1)
        results = Item.query.all()

        # проверяем, есть ли вообще значения в таблице
        if results:
            last_valuation = results[-1]

            # если последняя запись была больше часа назад, тогда удаляем все значения таблицы
            if (last_valuation.time < past_hour):
                results = Item.query.all()
                for item in results:
                    db.session.delete(item)
                db.session.commit()


            # если последняя запись была менее часа назад, тогда удаляем всё лишнее и заполняем пробелы нулями
            else:

                results = Item.query.filter(Item.time < past_hour).all()
                past_bool = len(results) > 0
                for item in results:
                    db.session.delete(item)
                db.session.commit()

                results = Item.query.all()
                last_valuation = results[-1]
                dif_of_times = datetime.datetime.now() - last_valuation.time
                timed_10_sec = datetime.timedelta(seconds=10)

                # если последняя запись более 10 сек назад, то ставим 0
                if dif_of_times > timed_10_sec:

                    timed_5_sec = datetime.timedelta(seconds=5)
                    timed_now = datetime.datetime.now()
                    # пока сервер спал, время шло. Мы считаем это время по 5 сек на отрезок
                    while dif_of_times > timed_5_sec:

                        if dif_of_times > datetime.timedelta(seconds=10):
                            # тут вычитаем время и прибавляем 5 секунд, чтобы не повторяться
                            item = Item(cpuload=str(0), time=(timed_now - dif_of_times) + timed_5_sec)

                        db.session.add(item)
                        # тут сокращаем время на 5 секунд
                        dif_of_times -= timed_5_sec

                        if dif_of_times <= timed_5_sec:
                            db.session.commit()

                # если были записи, от которых мы избавились
                if past_bool:

                    # заново ищем все элементы, чтобы их отсортировать по id
                    results = Item.query.all()
                    b = 1
                    for item in results:
                        item.id = b
                        b += 1
                    db.session.commit()

        # вычисляем % load cpu
        interval = psutil.cpu_percent()
        # вставляем в таблицу
        item = Item(cpuload=str(interval), time=datetime.datetime.now())
        db.session.add(item)
        db.session.commit()

        results = Item.query.filter(Item.time > past_hour).order_by(desc(Item.id)).all()

        message = {}

        limitation = 1
        for item in results:
            if limitation < 721:
                message[item.id] = item.cpuload
                limitation += 1
        return jsonify(message)  # сериализация JSON


if __name__ == "__main__":
    app.run(host="0.0.0.0")
